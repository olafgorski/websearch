#!/usr/bin/env python
import sys, pyperclip, webbrowser, os, signal
# On Linux, pyperclip module makes use of the xclip or xsel command


def main():
    close = False
    if '-e' in sys.argv:
        del sys.argv[sys.argv.index('-e')]
        close = True
    if len(sys.argv) == 2:
        if sys.argv[1] == '-h':
            print(
                " This is a simple script that let's you open given url in new tab.",
                "\n Usage: \n websearch.py search_engine search_term\n",
                "search_engine in optional, by default it's google\n",
                "available ones: ['google', 'duckduckgo']\n",
                "search_term is any query you might want to search\n\n",
                "Available flags:\n",
                "websearch.py -h : \n \twill cause the display of manual\n",
                "websearch.py -c : \n \twill allow you to get the url from clipboard"
                "\n\tand automatically search with default search_engine\n"
            )
        elif sys.argv[1] == '-c':
            term = pyperclip.paste()
            search(term, close)
        else:
            search(sys.argv[1])
    elif len(sys.argv) == 3:
        search(sys.argv[2], sys.argv[1])
    else: print("Wrong usage, for help pls use -h flag")


def search(term, close=False, search_engine='google'):
    search_engines = {
        'google': 'https://www.google.pl/search?q=',
        'duckduckgo': 'https://duckduckgo.com/?q=',
                      }
    if search_engine in search_engines.keys():
        url = search_engines[search_engine]+term.strip().replace(' ', '+')
        webbrowser.open_new_tab(url)
        if close:
            os.kill(os.getppid(), signal.SIGHUP)
    else:
        print("Search engine not available\n")


if __name__ == "__main__":
    main()
