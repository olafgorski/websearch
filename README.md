# Websearch.py
This is a tiny tiny script that I wrote mainly for myself and just for fun

It's main purpose is quite simple - to open up a link you have in your clipboard in new tab in your current browser or new browser window if your browser is closed.

I bound it's execution to key combination so now every time I want to open link from a txt file or sth, I can do it faster. Saving 2 keystrokes each time! Neat, huh? 

# Installing
No need to. Just use it `python websearch.py`

# Usage 
`websearch.py search_engine search_term` - search_engine is optional, by default it's google 

available ones: ['google', 'duckduckgo']

`search_term` - is any query you might want to search

# Available flags:
`websearch.py -h` - will cause the display of manual

`websearch.py -c` - will allow you to get the url from clipboard and automatically search with default search_engine

`websearch.py -e` - will cause the current terminal window to close after the script finishes

# Make it exectuable
To make it exectuable (if u wanna make a shortcut for this script for example) just use `chmod a+x websearch.py' all done.

# Dependencies
To paste from clipboard, I used [pyperclip](https://github.com/asweigart/pyperclip), which in order to work on linux needs xclip and/or xsel installed. For details check it's github page.

# License
Free. Do whatever.

